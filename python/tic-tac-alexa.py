import os
import random
import math
import time
import sys
# The rgbmatrix package is not installed in a standard location.
# Add the location to the system path.
sys.path.append('/home/elea/rpi-rgb-led-matrix/bindings/python/rgbmatrix')
from rgbmatrix import RGBMatrix, RGBMatrixOptions, graphics

# Initialize the RGB matrix per the sample code.
options = RGBMatrixOptions()
options.rows = 32
options.cols = 32
options.chain_length = 1
options.parallel = 1
options.hardware_mapping = 'adafruit-hat'
options.row_address_type = 0
options.pwm_bits = 11
options.brightness = 100
options.pwm_lsb_nanoseconds = 130
options.led_rgb_sequence = "RGB"
options.pixel_mapper_config = ""
options.panel_type = ""
matrix = RGBMatrix(options=options)



# Commands from Alex will be written to this file
command_file = "/home/elea/cmd.sh"


def get_command_time():
    try:
        return os.path.getmtime(command_file)
    except FileNotFoundError:
        return None

last_modified_time = get_command_time()


def draw_x(canvas, col_ind, row_ind, color):
    # This function draws an X in the box.
    cell_size = 32 // 3
    
    x0 = col_ind * 11 + 1
    y0 = row_ind * 11 + 1
    x1 = x0 + 7
    y1 = y0 + 7
    graphics.DrawLine(canvas, x0, y0, x1, y1, color)
    graphics.DrawLine(canvas, x0, y1, x1, y0, color)

def draw_o(canvas, col_ind, row_ind, color):
    # This function draws an O in the box.
    cell_size = 32 // 3
    center_x = col_ind * cell_size + cell_size // 2
    center_y = row_ind * cell_size + cell_size // 2
    radius = (cell_size // 2) - 1
    graphics.DrawCircle(canvas, center_x, center_y, radius, color)

def draw_tic_tac_toe_board(matrix):
    # This function draws our board to a canvas.
    green = graphics.Color(0, 255, 0)
    canvas = matrix.CreateFrameCanvas()
    cell_size = 32 // 3
    for i in range(1, 3):
        x = y = cell_size * i
        graphics.DrawLine(canvas, x, 0, x, 31, green)
        graphics.DrawLine(canvas, 31, y, 0, y, green)
    matrix.SwapOnVSync(canvas)
    return canvas

def check_winner(board, player):
    # Check our board against all possible winning moves.
    winning_combinations = [(0, 1, 2), (3, 4, 5), (6, 7, 8),
                            (0, 3, 6), (1, 4, 7), (2, 5, 8),
                            (0, 4, 8), (2, 4, 6)]
    for combo in winning_combinations:
        if all(board[i] == player for i in combo):
            return True
    return False


def get_computer_move(board):
    # Return a random square that does not have an X or O
    return random.choice([i for i, x in enumerate(board) if x == " "])

def get_player_move(board):
    global last_modified_time
    # The methodology to check for the file modification time was 
    # found here: https://www.tutorialspoint.com/How-to-monitor-Python-files-for-changes
    while True:
        modification_time = get_command_time()
        if modification_time is None or (last_modified_time is not None and modification_time <= last_modified_time):
            # Sleep for a second to reduce CPU overhead
            time.sleep(1)
            continue
        last_modified_time = modification_time
        with open(command_file, "r") as file:
            command = file.read().strip()
            if command.startswith("X"):
                move = int(command[1]) - 1
                if 0 <= move < 9 and board[move] == " ":
                    return move
        # Sleep for a second to reduce CPU overhead
        time.sleep(1)
        continue

def main():
    global last_modified_time
    board = [" " for _ in range(9)]
    canvas = draw_tic_tac_toe_board(matrix)
    
    # If the file exists, get the last modification time.  We don't want to re-use
    # this pre-existing command.
    last_modified_time = get_command_time()


    while True:
        # Player's Turn
        move = get_player_move(board)
        board[move] = "X"
        draw_x(canvas, move % 3, move // 3, graphics.Color(255, 0, 0))
        # Move the canvas to our RGB display
        matrix.SwapOnVSync(canvas)

        if check_winner(board, "X"):
            print("Congratulations! You've won!")
            break

        # Check for a tie.  There are an odd number of boxes so a tie needs
        # to only be checked here, not after computer move.
        if all(space != " " for space in board):
            print("It's a tie!")
            break

        # Computer's Turn.  Add a small delay to give the illusion of thinking
        time.sleep(1)

        computer_move = get_computer_move(board)
        board[computer_move] = "O"
        draw_o(canvas, computer_move % 3, computer_move // 3, graphics.Color(0, 0, 255))
        # Move the canvas to our RGB display
        matrix.SwapOnVSync(canvas)

        if check_winner(board, "O"):
            print("Computer wins!")
            break
    time.sleep(5)


if __name__ == "__main__":
    main()

