import math
import time
import sys
import random
# The rgbmatrix package is not installed in a standard location.
# Add the location to the system path.
sys.path.append('/home/elea/rpi-rgb-led-matrix/bindings/python/rgbmatrix')
from rgbmatrix import RGBMatrix, RGBMatrixOptions, graphics

# Initialize the RGB matrix per the sample code.
options = RGBMatrixOptions()
options.rows = 32
options.cols = 32
options.chain_length = 1
options.parallel = 1
options.hardware_mapping = 'adafruit-hat'
options.row_address_type = 0
options.pwm_bits = 11
options.brightness = 100
options.pwm_lsb_nanoseconds = 130
options.led_rgb_sequence = "RGB"
options.pixel_mapper_config = ""
options.panel_type = ""
matrix = RGBMatrix(options=options)


# Function to draw a spiral
def draw_spiral(matrix, loops=4):
    center_x, center_y = matrix.width // 2, matrix.height // 2
    spacing = 2 * math.pi / matrix.width  # Adjust the spacing for a tighter or looser spiral

    for i in range(matrix.width * loops):
        angle = i * spacing
        radius = i / (matrix.width * loops / center_x)
        x = int(center_x + radius * math.cos(angle))
        y = int(center_y + radius * math.sin(angle))

        # Generate a random color
        color = graphics.Color(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

        if 0 <= x < matrix.width and 0 <= y < matrix.height:
            
            matrix.SetPixel(x, y, color.red, color.green, color.blue)
            time.sleep(0.1)
        else:
            break

# Clear matrix
matrix.Clear()

# Define color
color = graphics.Color(255, 0, 0)  # Red color

# Draw spiral
draw_spiral(matrix)

# Keep the image displayed
time.sleep(10)
matrix.Clear()
